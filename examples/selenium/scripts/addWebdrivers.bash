#! /usr/bin/env bash
set -euo pipefail

export JMETER_WEBDRIVERS="${JMETER_HOME}/webdrivers"
mkdir -p ${JMETER_WEBDRIVERS} \
  && cd ${JMETER_WEBDRIVERS} 

chromedriverVer=$(curl --silent https://chromedriver.storage.googleapis.com/LATEST_RELEASE)
echo "get webdriver for chrome, version [${chromedriverVer}]"
curl --remote-name --location --silent https://chromedriver.storage.googleapis.com/${chromedriverVer}/chromedriver_linux64.zip
unzip -q chromedriver_linux64.zip
rm chromedriver_linux64.zip LICENSE.chromedriver

geckodriverVer=$(curl --silent https://github.com/mozilla/geckodriver/releases | grep 'linux64.tar.gz"' | grep href | sed 's+.*download/v++;s+/.*++')
echo "get webdriver for firefox, version [${geckodriverVer}]"
curl --remote-name --location --silent https://github.com/mozilla/geckodriver/releases/download/v${geckodriverVer}/geckodriver-v${geckodriverVer}-linux64.tar.gz
tar -zxf geckodriver-v${geckodriverVer}-linux64.tar.gz
rm geckodriver-v${geckodriverVer}-linux64.tar.gz

edgedriverVer=$(curl --silent https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/ | grep linux | sed 's+/edgedriver_linux64.zip".*++;s+.*/++' | head -n1)
echo "get webdriver for edge, version [${edgedriverVer}]"
curl --remote-name --location --silent https://msedgedriver.azureedge.net/${edgedriverVer}/edgedriver_linux64.zip
unzip -q edgedriver_linux64.zip
rm edgedriver_linux64.zip
