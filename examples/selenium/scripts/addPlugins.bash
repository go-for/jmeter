#! /usr/bin/env bash
set -euo pipefail

# record status before installation
find ${JMETER_HOME}/ > plugins.install.before.log

cd ${JMETER_HOME} \
  && ./bin/PluginsManagerCMD.sh install jpgc-webdriver 

cd -

# record status after installation
find ${JMETER_HOME}/ > plugins.install.after.log

# log difference and cleanup
diff plugins.install.after.log plugins.install.before.log > plugins.install.diff.log
rm plugins.install.after.log plugins.install.before.log
