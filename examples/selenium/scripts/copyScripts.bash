#! /usr/bin/env bash
set -euo pipefail

cp ../scenarios/*.jmx ${JMETER_SCENARIOS}
