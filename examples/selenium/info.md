##### Setup of selenium with JMeter

 * install plugin manger:
   * go to https://jmeter-plugins.org/install/Install/
   * dowload latest version to lib/ext in jmeter base directory from https://jmeter-plugins.org/get/
   * restart JMeter
 * go to options->download , search in available plugins for: Selenium/WebDriver support
 * tick it for installation, and click button `Apply Changes and Restart JMeter`
 * it should install for you:
   * selenium-chrome-driver
   * selenium-remote-driver
   * selenium-firefox-driver
   * selenium-ie-driver
   * selenium-edge-driver
   * selenium-chromium-driver


##### Set up of browsers

To user browsers for load driverving you need... browsers. They should be installed so they can be contacted by webdrivers, called by jmeter.

    JMeter <-> webdriver plugin <-> webdrivers <-> browsers <-> remote host

Make sure that your browser binaries are in your env PATH, so wedrivers can start them when needed.

You can download brosers:

 * chrome: https://www.google.com/chrome/?platform=linux
 * firefox: https://www.mozilla.org/en-US/firefox/all/#product-desktop-release

You can also install browsers with you system package installer.

##### Set up of webdrivers

JMeter selenium plugin needs path to webdriver executable, to proceed. Here goes more information: https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/

You need to match version of web driver with version of you browser binary installed on the system.

Here are some examples, which can be used for download:

 * chrome: https://chromedriver.storage.googleapis.com/index.html
 * firefox: https://github.com/mozilla/geckodriver/releases
 * edge: https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
 * safari: built-in (?, not verified)


##### Recording with Selenium IDE and browser

Example of how to record basic scenario:

 * add Selenium IDE plugin to your browser -> plugin is added to browser
 * click on plugin icon -> Selenium IDE window opens
 * click on `Record a new test in a new project`
 * enter name for it (e.g. test1)
 * enter base URL to start recording (e.g. https://jmeter-plugins.org) -> new window opens
 * click a few links
 * close opened window -> under test section `Untitled` appears
 * export untitled test as Java JUnit -> file with java code is created
 * you can use in JMeter code in `@Test` part of the exported code, also mind imports, as some of them may be also needed


##### Working setup

Versions of tools tested in this example in linux environment

 * `./chrome --version` -> Google Chrome 109.0.5414.74 unknown
 * `./firefox --version` -> Mozilla Firefox 109.0
 * `./geckodriver --version` -> geckodriver 0.32.0 (4563dd583110 2022-10-13 09:22 +0000)
 * `./chromedriver --version` -> ChromeDriver 109.0.5414.74 (e7c5703604daa9cc128ccf5a5d3e993513758913-refs/branch-heads/5414@{#1172})


##### Other resources

Refrerences

 * [source code of jmeter-plugins-webdriver](https://github.com/undera/jmeter-plugins-webdriver)
 * [geckodriver capabilities](https://developer.mozilla.org/en-US/docs/Web/WebDriver/Commands)
 * [chromedriver capabilities](https://chromium.googlesource.com/chromium/src/+/master/docs/chromedriver_status.md)

Examples:

 * [basic actions examples](https://blog.kobiton.com/jmeter-selenium-webdriver-integration)
 * [selenium code examples](https://github.com/PacktPublishing/Selenium-WebDriver-3-Practical-Guide-Second-Edition)

Tutorials

 * [introduction to selenium webdriver](https://www.blazemeter.com/blog/what-is-selenium-webdriver)
 * [webdriver commands reference](https://www.blazemeter.com/blog/selenium-web-driver-commands)
 * [example set to be used for jmeter and selenium (docker images)](https://www.blazemeter.com/blog/best-docker-images)
