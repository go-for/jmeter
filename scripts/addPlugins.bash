#! /usr/bin/env bash
set -euo pipefail

cd ${JMETER_HOME} \
  && ./bin/PluginsManagerCMD.sh install jpgc-casutg,jpgc-functions

