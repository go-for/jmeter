#! /usr/bin/env bash
set -euo pipefail

cd ${JMETER_HOME}/bin;
${JAVA_HOME}/bin/java \
 -jar ApacheJMeter.jar \
 -L INFO \
 -J JMETER_LOG_BASE=${JMETER_LOGS} \
 -j ${JMETER_LOGS}/jmeter.run.log \
 -l ${JMETER_LOGS}/jmeter.jtl.log \
 -n \
 "$@" \
 1>${JMETER_LOGS}/jmeter.stdout.log \
 2>${JMETER_LOGS}/jmeter.stderr.log
