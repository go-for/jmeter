#! /usr/bin/env bash
set -euo pipefail

logFile="${JMETER_LOGS}/jmeter.jtl.log"
test -f ${logFile} || { echo "File not found [${logFile}]"; exit 1; }

reportDir=${JMETER_REPORTS}/$(date +'%Y.%m.%d_%H.%M.%S')
mkdir -p ${reportDir}

cd ${JMETER_HOME}/bin
${JAVA_HOME}/bin/java \
 -jar ApacheJMeter.jar \
 -g ${logFile} \
 -o ${reportDir}

echo "report created in [${reportDir}]"
