#! /usr/bin/env bash

test -z "${JAVA_OPTS}" && JAVA_OPTS=' '

set -euo pipefail

cd ${JMETER_HOME}/bin;
${JAVA_HOME}/bin/java \
 ${JAVA_OPTS} \
 -jar ApacheJMeter.jar \
 -L INFO \
 -J JMETER_LOG_BASE=${JMETER_LOGS} \
 -j ${JMETER_LOGS}/jmeter.run.log \
 -l ${JMETER_LOGS}/jmeter.jtl.log \
 "$@" \
 1>${JMETER_LOGS}/jmeter.stdout.log \
 2>${JMETER_LOGS}/jmeter.stderr.log
