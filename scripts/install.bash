#! /usr/bin/env bash
set -euo pipefail

# go to right place
cd ${JMETER_BASE}

echo "getting latest jmeter [${JMETER_VERSION}] to [${JMETER_BASE}] ..."
curl --silent https://downloads.apache.org/jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz | tar zxf - \
  && cd apache-jmeter-${JMETER_VERSION} \
  && rm -rf docs extras LICENSE licenses NOTICE printable_docs README.md \
  && mv ${JMETER_BASE}/apache-jmeter-${JMETER_VERSION}/* ${JMETER_HOME}

echo "getting plugin manager ..."
curl --silent --location --output ${JMETER_HOME}/lib/ext/plugin-manager.jar https://jmeter-plugins.org/get/

echo "geting tool to install plugins from command line [cmdrunner-${JMETER_CMDRUNNER_VERSION}]"
curl --silent --location --output ${JMETER_HOME}/lib/cmdrunner-${JMETER_CMDRUNNER_VERSION}.jar http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/${JMETER_CMDRUNNER_VERSION}/cmdrunner-${JMETER_CMDRUNNER_VERSION}.jar

# create PluginsManagerCMD.sh & PluginsManagerCMD.bat files
echo "initiating script to install plugins"
java -cp ${JMETER_HOME}/lib/ext/plugin-manager.jar org.jmeterplugins.repository.PluginManagerCMDInstaller
