#! /usr/bin/env bash
set -euo pipefail

cd ${JMETER_HOME}/bin;
${JAVA_HOME}/bin/java \
 -jar ApacheJMeter.jar \
 -L DEBUG \
 -J JMETER_LOG_BASE=${JMETER_LOGS} \
 -J jmeter.save.saveservice.output_format=xml \
 -J jmeter.save.saveservice.response_data=true \
 -J jmeter.save.saveservice.response_data.on_error=false \
 -J jmeter.save.saveservice.responseHeaders=true \
 -J jmeter.save.saveservice.samplerData=true \
 -J jmeter.save.saveservice.requestHeaders=true \
 -J jmeter.save.saveservice.url=true \
 -j ${JMETER_LOGS}/jmeter.run.log \
 -l ${JMETER_LOGS}/jmeter.jtl.log \
 -n \
 "$@" \
 1>${JMETER_LOGS}/jmeter.stdout.log \
 2>${JMETER_LOGS}/jmeter.stderr.log
