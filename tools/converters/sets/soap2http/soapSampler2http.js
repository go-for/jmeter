/*jslint white: true */
/*jslint es6 */

// from jmeter elements
/*global printHTTPSamplerProxy */
/*global printHeader */
/*global printHeaderManager */
/*global printPostDataParam */

// browser
/*global document */

"use strict";

//----------------------------------------------------
function createHttpFromSoap(samplerName,samplerEnabled,soapEndpoint,soapPostData){
 
 //console.log ( "[" + samplerName + "]" );
 //console.log ( "[" + samplerEnabled + "]" );
 //console.log ( "[" + soapEndpoint + "]" );
 //console.log ( "[" + soapPostData + "]" );

 var httpNodeTxt = '' +
  '<HTTPSamplerProxy guiclass="HttpTestSampleGui" testclass="HTTPSamplerProxy" testname="' + encodeXml(samplerName) + '" enabled="' + encodeXml(samplerEnabled) + '">' +
   '<boolProp name="HTTPSampler.postBodyRaw">true</boolProp>' + 
   '<elementProp name="HTTPsampler.Arguments" elementType="Arguments">' + 
     '<collectionProp name="Arguments.arguments">' + 
       '<elementProp name="" elementType="HTTPArgument">' + 
         '<boolProp name="HTTPArgument.always_encode">false</boolProp>' + 
          '<stringProp name="Argument.value">' + encodeXml(soapPostData) + '</stringProp>' +
         '<stringProp name="Argument.metadata">=</stringProp>' + 
       '</elementProp>' + 
     '</collectionProp>' + 
   '</elementProp>' + 
   '<stringProp name="HTTPSampler.domain"></stringProp>' + 
   '<stringProp name="HTTPSampler.port"></stringProp>' + 
   '<stringProp name="HTTPSampler.protocol"></stringProp>' + 
   '<stringProp name="HTTPSampler.contentEncoding"></stringProp>' + 
   '<stringProp name="HTTPSampler.path">' + encodeXml(soapEndpoint) + '</stringProp>' + 
   '<stringProp name="HTTPSampler.method">POST</stringProp>' + 
   '<boolProp name="HTTPSampler.follow_redirects">true</boolProp>' + 
   '<boolProp name="HTTPSampler.auto_redirects">false</boolProp>' + 
   '<boolProp name="HTTPSampler.use_keepalive">true</boolProp>' + 
   '<boolProp name="HTTPSampler.DO_MULTIPART_POST">false</boolProp>' + 
   '<stringProp name="HTTPSampler.embedded_url_re"></stringProp>' + 
   '<stringProp name="HTTPSampler.connect_timeout"></stringProp>' + 
   '<stringProp name="HTTPSampler.response_timeout"></stringProp>' + 
  '</HTTPSamplerProxy>' ; 

  return httpNodeTxt;

}

//----------------------------------------------------
function createHttpHeaderSoap(soapAction){

 var httpHeaderNodeTxt = '' +
  '<HeaderManager guiclass="HeaderPanel" testclass="HeaderManager" testname="soap header" enabled="true">' + 
    '<collectionProp name="HeaderManager.headers">' + 
      '<elementProp name="" elementType="Header">' + 
        '<stringProp name="Header.name">Content-Type</stringProp>' + 
        '<stringProp name="Header.value">text/xml; charset=utf-8</stringProp>' + 
      '</elementProp>' + 
      '<elementProp name="" elementType="Header">' + 
        '<stringProp name="Header.name">SOAPAction</stringProp>' + 
        '<stringProp name="Header.value">' + encodeXml(soapAction) + '</stringProp>' + 
      '</elementProp>' + 
    '</collectionProp>' + 
  '</HeaderManager>';

 return httpHeaderNodeTxt;

}

//----------------------------------------------------
function updateSoapNode (node){

 var samplerName = "",
     samplerEnabled = "",
     samplerProperties = null,
     soapAction = "",
     soapEndpoint = "",
     soapPostData = "",
     httpNodeTxt = "",
     outputLog = "";

 //console.log("... updating");
 samplerName = node.attributes.testname.value;
 samplerEnabled = node.attributes.enabled.value;
 //console.log ( "[" + samplerName + "]" );
 //console.log ( "[" + samplerEnabled + "]" );

 samplerProperties = node.childNodes;
 for (var j = 0; j < samplerProperties.length; j ++){
  if (samplerProperties[j].nodeName == "stringProp"){
   if (samplerProperties[j].attributes.name.value == "SoapSampler.SOAP_ACTION") {
    soapAction = samplerProperties[j].textContent;
   }
   if (samplerProperties[j].attributes.name.value == "SoapSampler.URL_DATA") {
    soapEndpoint = samplerProperties[j].textContent;
   }
   if (samplerProperties[j].attributes.name.value == "HTTPSamper.xml_data") {
    soapPostData = samplerProperties[j].textContent;
   }
  }
 }
 //console.log ( "[" + soapAction + "]" );
 //console.log ( "[" + soapEndpoint + "]" );
 //console.log ( "[" + soapPostData + "]" );

 httpNodeTxt = createHttpFromSoap(samplerName,samplerEnabled,soapEndpoint,soapPostData);
 outputLog = "soap sampler [" + samplerName + "], soap action [" + soapAction + "]\n";

 node.outerHTML = httpNodeTxt;

 return [soapAction,outputLog];

}

//----------------------------------------------------
function updateHashNode (node,soapAction){

  //console.log("hash node found");
  //console.log(node.outerHTML);
  if (node.innerHTML == ""){
   node.outerHTML = "<hashTree>" + createHttpHeaderSoap(soapAction) + "<hashTree/>" + "</hashTree>";
  }else{
   node.innerHTML = createHttpHeaderSoap(soapAction) + "<hashTree/>" + node.innerHTML;
  }
 
}

//----------------------------------------------------
function checkNodes (nodes){

 var result=null,
     soapAction = "",
     outputLog = "";

 for (var i = 0; i < nodes.length; i++) {
  //console.log("checking [" + nodes[i].nodeName + "]");
  if (nodes[i].nodeName == "SoapSampler"){
   //console.log("[SoapSampler] found");
   result = updateSoapNode(nodes[i]);
   soapAction = result[0];
   outputLog = outputLog + result[1];

   if (nodes[i+1].nodeName == "hashTree"){
    //console.log (nodes[i+1].nodeName);
    updateHashNode(nodes[i+1],soapAction);
   }else{
    //console.log (nodes[i+2].nodeName);
    updateHashNode(nodes[i+2],soapAction);
   }

  }else if (nodes[i].childNodes.length>0){
   //console.log("[" + nodes[i].nodeName + "] is parent");
   outputLog = outputLog + checkNodes (nodes[i].childNodes);
  }
 }

 return outputLog;
 
}

//----------------------------------------------------
// MAIN LOGIC
///----------------------------------------------------

//----------------------------------------------------
function soapSampler2http(input) {

 var parser = new DOMParser(),
     xml = null,
     xmlTmp = null,
     soapNodes = null,
     httpNode = null,
     httpNodeTxt = "",
     outputLog = "",
     outputJmx = "",
     samplerName = "",
     samplerEnabled = "",
     samplerProperties = null,
     soapEndpoint = "",
     soapPostData = "",
     soapAction = "",
     defaultComment = "generated with har2jmx javascript";

 if (window.DOMParser)
 {
     parser = new DOMParser();
     xml = parser.parseFromString(input, "text/xml");
 }
 else // Internet Explorer
 {
     xml = new ActiveXObject("Microsoft.XMLDOM");
     xml.async = false;
     xml.loadXML(input);
 }


 outputLog = checkNodes(xml.documentElement.childNodes);
 outputJmx = new XMLSerializer().serializeToString(xml);

 return [outputLog,outputJmx];
}

